import { Post, User } from "../repositories/mod.ts";

const testUserId = Deno.env.get("ENC22_TEST_UID") ?? "test-uid";
const testUserName = Deno.env.get("ENC22_TEST_NAME") ?? "testuser";

const posts: Post[] = [
  { "day": 1, "name": "arctic-wolf", "author_id": testUserId },
  { "day": 2, "name": "ethiopian-wolf", "author_id": testUserId },
  { "day": 1, "name": "gray-wolf", "author_id": "test-uid-2" },
  { "day": 1, "name": "indian-wolf", "author_id": "test-uid-2" },
  { "day": 1, "name": "rad-wolf", "author_id": "test-uid-2" },
  { "day": 1, "name": "spacex", "author_id": "test-uid-2" },
  { "day": 1, "name": "red-wolf", "author_id": "test-uid-2" },
];
const users: User[] = [
  { uid: testUserId, name: testUserName },
  { uid: "test-uid-2", name: "testuser2" },
];

export async function setDevData(Kv: Deno.Kv) {
  await Promise.all(posts.map((post) => Kv.set(["posts", post.name], post)));
  await Promise.all(
    users.map((user) => Kv.set(["users", user.uid], user)),
  );
}
