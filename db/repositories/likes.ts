import { Kv } from "../client.ts";

export type Like = { post_name: string; user_id: string };

export async function getLikesForPost(postName: string) {
  const likes: Like[] = [];
  for await (const like of Kv.list<Like>({ prefix: ["likes", postName] })) {
    likes.push(like.value);
  }
  return likes;
}
export async function likeExists(postName: string, uid: string) {
  const like = await Kv.get<Like>(["likes", postName, uid]);
  return like.value !== null;
}

export function addLike(like: Like) {
  return Kv.set(["likes", like.post_name, like.user_id], like);
}

export function deleteLike(postName: string, uid: string) {
  return Kv.delete(["likes", postName, uid]);
}
