import { Kv } from "../client.ts";

export type User = { uid: string; name: string };

export async function getAllUsers() {
  const users: User[] = [];
  for await (const user of Kv.list<User>({ prefix: ["users"] })) {
    users.push(user.value);
  }
  return users;
}

export async function getUserById(uid: string) {
  const user = await Kv.get<User>(["users", uid]);
  return user.value;
}

export function createUser(user: User) {
  return Kv.set(["users", user.uid], user);
}
