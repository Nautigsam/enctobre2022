import { Handlers, PageProps } from "$fresh/server.ts";
import {
  getAllPosts,
  getAllThemesAsDict,
  getAllUsers,
  getLikesForPost,
} from "../../db/repositories/mod.ts";
import { PostsGrid, PostsGridItem } from "../../components/PostsGrid.tsx";
import PageState from "../../utils/page-state.ts";
import { ImagesStore } from "../../utils/images-store.ts";

interface Day {
  number: number;
  theme: {
    name: string;
    previous: { number: number; name: string } | null;
    next: { number: number; name: string } | null;
  };
  posts: PostsGridItem[];
}

export const handler: Handlers<Day | null, PageState> = {
  async GET(_, ctx) {
    const { day: dayStr } = ctx.params;
    const dayNum = parseInt(dayStr);
    if (isNaN(dayNum) || dayNum > 31) {
      // TODO(abertron): Replace with ctx.renderNotFound() once it's available (1.1.x maybe?).
      return new Response(null, { status: 303, headers: { "location": "/" } });
    }
    const users = await getAllUsers();
    const collator = new Intl.Collator();
    const unsortedPosts: PostsGridItem[] = await Promise.all(
      (await getAllPosts()).filter((post) => post.day === dayNum)
        .map(
          async (
            post,
          ) => {
            const author = users.find((u) => u.uid === post.author_id)?.name ??
              "inconnu";
            const likes = await getLikesForPost(post.name);
            const isLiked = likes.some((like) =>
              like.user_id === ctx.state.user?.uid
            );
            const imageName = `${post.name}.jpg`;
            return {
              ...post,
              author,
              text: author,
              link: `/users/${post.author_id}`,
              isLiked,
              likesCount: likes.length,
              originalUrl: ImagesStore.getUrl("originals", imageName),
              croppedUrl: ImagesStore.getUrl("cropped", imageName),
            };
          },
        ),
    );
    const posts = unsortedPosts.sort((post1, post2) =>
      collator.compare(post1.author, post2.author)
    );
    const themes = await getAllThemesAsDict();

    const previousNumber = dayNum - 1;
    const nextNumber = dayNum + 1;
    const theme = {
      name: themes[dayNum],
      previous: themes[previousNumber]
        ? { number: previousNumber, name: themes[previousNumber] }
        : null,
      next: themes[nextNumber]
        ? { number: nextNumber, name: themes[nextNumber] }
        : null,
    };

    const day: Day = {
      number: dayNum,
      theme,
      posts,
    };

    ctx.state.title = `Jour ${day.number} - ${day.theme.name}`;

    return ctx.render(day);
  },
};

export default function ShowDay({ data: day }: PageProps<Day>) {
  const previous = day.theme.previous;
  const next = day.theme.next;
  return (
    <>
      <nav class="my-5 flex flex-row justify-between items-center text-sm md:text-md md:max-w-[50%] md:mx-auto">
        <div class="w-[25%]">
          {previous
            ? (
              <div class="flex flex-row justify-start gap-2">
                <span>{"<"}</span>
                <a
                  class="max-w-full truncate underline"
                  href={`/days/${previous.number}`}
                >
                  {previous.name}
                </a>
              </div>
            )
            : ""}
        </div>

        <h1 class="text-xl justify-self-center md:text-2xl">
          {day.number}. {day.theme.name}
        </h1>

        <div class="w-[25%] text-right">
          {next
            ? (
              <div class="flex flex-row justify-end gap-2">
                <a
                  class="max-w-full truncate underline"
                  href={`/days/${next.number}`}
                >
                  {next.name}
                </a>
                <span>{">"}</span>
              </div>
            )
            : ""}
        </div>
      </nav>
      {day.posts.length === 0
        ? (
          <div class="mt-5 mx-auto text-center">
            <p>
              Aucune image n'a encore été postée pour ce jour.
            </p>
            <a href="/" class="underline">
              Retourner à la grille principale
            </a>
          </div>
        )
        : <PostsGrid posts={day.posts} />}
    </>
  );
}
