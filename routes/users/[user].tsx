import { Handlers, PageProps } from "$fresh/server.ts";
import { getAllPosts, getAllThemesAsDict } from "../../db/repositories/mod.ts";
import { PostsGrid, PostsGridItem } from "../../components/PostsGrid.tsx";
import PageState from "../../utils/page-state.ts";
import { getLikesForPost, getUserById } from "../../db/repositories/mod.ts";
import { ImagesStore } from "../../utils/images-store.ts";

interface User {
  name: string;
  posts: PostsGridItem[];
  currentUser: PageState["user"];
}

export const handler: Handlers<User | null, PageState> = {
  async GET(_, ctx) {
    const { user: userId } = ctx.params;
    const userInfo = await getUserById(userId);
    if (!userInfo || !userInfo.name) {
      return ctx.renderNotFound();
    }
    const themes = await getAllThemesAsDict();
    const unsortedPosts: PostsGridItem[] = await Promise.all(
      (await getAllPosts()).filter((post) => post.author_id === userId)
        .map(
          async (post) => {
            const likes = await getLikesForPost(post.name);
            const isLiked = likes.some((like) =>
              like.user_id === ctx.state.user?.uid
            );
            const imageName = `${post.name}.jpg`;
            return {
              ...post,
              author: userInfo.name,
              text: `${post.day}. ${themes[post.day]}`,
              link: `/days/${post.day}`,
              isLiked,
              likesCount: likes.length,
              originalUrl: ImagesStore.getUrl("originals", imageName),
              croppedUrl: ImagesStore.getUrl("cropped", imageName),
            };
          },
        ),
    );
    const posts = unsortedPosts.sort((post1, post2) => post1.day - post2.day);
    const user: User = {
      name: userInfo.name,
      posts,
      currentUser: ctx.state.user,
    };

    ctx.state.title = user.name;

    return ctx.render(user);
  },
};

export default function ShowUser({ data: user }: PageProps<User>) {
  return (
    <>
      <h1 class="my-5 text-xl md:text-2xl font-bold">
        Images de {user.name}
      </h1>
      {user.posts.length === 0
        ? (
          <div class="mt-5 mx-auto text-center">
            <p>
              Cet utilisateur n'a pas encore posté d'images.
            </p>
            <a href="/" class="underline">
              Retourner à la grille principale
            </a>
          </div>
        )
        : <PostsGrid posts={user.posts} />}
    </>
  );
}
