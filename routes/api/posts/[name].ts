import { Handlers } from "$fresh/server.ts";
import { likeExists } from "../../../db/repositories/likes.ts";
import {
  addLike,
  deleteLike,
  deletePost,
  getPostByName,
} from "../../../db/repositories/mod.ts";
import { ImagesStore } from "../../../utils/images-store.ts";
import PageState from "../../../utils/page-state.ts";

enum PostActions {
  ToggleLike = "togglelike",
}

export const handler: Handlers<unknown, PageState> = {
  async DELETE(_req, ctx) {
    const user = ctx.state.user;
    if (!user) {
      return new Response(null, { status: 401 });
    }
    const { name } = ctx.params;
    const post = await getPostByName(name);
    if (!post) {
      return new Response(null, { status: 404 });
    }
    if (post.author_id !== user.uid) {
      return new Response(null, { status: 403 });
    }

    await deleteLike(name, user.uid);
    await deletePost(name);

    try {
      const imageName = `${name}.jpg`;
      await ImagesStore.remove("cropped", imageName);
      await ImagesStore.remove("originals", imageName);
    } catch (err) {
      if (!(err instanceof Deno.errors.NotFound)) {
        throw err;
      }
    }

    return new Response(JSON.stringify(post), {
      headers: { "Content-Type": "application/json" },
    });
  },
  async POST(req, ctx) {
    const user = ctx.state.user;
    if (!user) {
      return new Response(null, { status: 401 });
    }
    const url = new URL(req.url);
    const actionStr = url.searchParams.get("action");
    if (actionStr !== PostActions.ToggleLike) {
      return new Response(null, { status: 400 });
    }
    const { name } = ctx.params;
    const post = await getPostByName(name);
    if (!post) {
      return new Response(null, { status: 404 });
    }

    if (await likeExists(name, user.uid)) {
      await deleteLike(name, user.uid);
    } else {
      await addLike({ post_name: name, user_id: user.uid });
    }

    return new Response(null, { status: 200 });
  },
};
