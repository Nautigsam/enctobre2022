import { MiddlewareHandlerContext } from "$fresh/server.ts";
import { getFirebaseUserFromRequest } from "../utils/user.ts";
import PageState from "../utils/page-state.ts";
import { getUserById } from "../db/repositories/mod.ts";

const pagesNeedingUsers = [
  "/",
  "/api/*",
  "/days/*",
  "/login",
  "/posts/new",
  "/users/*",
];

export async function handler(
  req: Request,
  ctx: MiddlewareHandlerContext<PageState>,
) {
  const url = new URL(req.url);
  const needsUser = pagesNeedingUsers.some((p) => {
    if (p.endsWith("*")) {
      return url.pathname.startsWith(p.substring(0, p.length - 1));
    } else {
      return url.pathname === p;
    }
  });

  if (!needsUser) return ctx.next();

  const firebaseUser = await getFirebaseUserFromRequest(req);
  if (!firebaseUser) return ctx.next();

  const userInfo = await getUserById(firebaseUser.user_id);

  if (!userInfo || !userInfo.name) {
    return new Response(null, {
      status: 302,
      headers: {
        "Location": `/set-user-info?redirect=${url.pathname}`,
      },
    });
  }

  ctx.state.user = userInfo;

  return ctx.next();
}
