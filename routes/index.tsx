import { Handlers, PageProps } from "$fresh/server.ts";
import { getAllPosts, getAllThemesAsList } from "../db/repositories/mod.ts";
import PageState from "../utils/page-state.ts";

type HomeData = {
  months: { text: string; path: string; postsCount: number }[];
};

export const handler: Handlers<HomeData | null, PageState> = {
  async GET(_, ctx) {
    ctx.state.title = "Grille principale";

    const posts = await getAllPosts();
    const postsCountByDay = posts.reduce<Record<number, number>>((acc, p) => {
      if (acc[p.day]) {
        acc[p.day]++;
      } else {
        acc[p.day] = 1;
      }
      return acc;
    }, {});
    const themes = await getAllThemesAsList();
    const months = themes.map((t) => ({
      text: `${t.day} - ${t.name}`,
      path: `/days/${t.day}`,
      postsCount: postsCountByDay[t.day] ?? 0,
    }));
    return ctx.render({ months });
  },
};

export default function Home({ data }: PageProps<HomeData>) {
  return (
    <div class="my-3 grid md:grid-cols-4 gap-2">
      {data.months.map((m) => (
        <a
          href={m.path}
          class="rounded border p-2 flex justify-between hover:bg-gray-200 dark:hover:bg-gray-800"
        >
          <span>{m.text}</span>
          <div class="flex gap-2">
            <span class="text-gray-600 dark:text-gray-400">
              ({m.postsCount})
            </span>
            <span class="md:hidden">&#5171;</span>
          </div>
        </a>
      ))}
    </div>
  );
}
