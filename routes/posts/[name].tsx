import { Handlers, PageProps, RouteConfig } from "$fresh/server.ts";
import PostCloseButton from "../../islands/PostCloseButton.tsx";
import { ImagesStore } from "../../utils/images-store.ts";

interface Post {
  imgPath: string;
  imgName: string;
}

export const config: RouteConfig = {
  skipInheritedLayouts: true,
};

export const handler: Handlers<Post | null> = {
  GET(_, ctx) {
    const { name: imgName } = ctx.params;
    const post: Post = {
      imgName,
      imgPath: stripHostnameIfFile(
        ImagesStore.getUrl("originals", `${imgName}.jpg`),
      ),
    };
    return ctx.render(post);
  },
};

export default function ShowPost({ data: post }: PageProps<Post>) {
  return (
    <div class="w-screen h-screen dark dark:bg-black dark:text-white">
      <PostCloseButton />
      <div class="flex flex-col justify-center w-full h-full md:flex-row">
        <img
          src={post.imgPath}
          alt={`The image "${post.imgName}"`}
        />
      </div>
    </div>
  );
}

function stripHostnameIfFile(url: URL) {
  if (url.protocol === "file:") {
    return url.pathname;
  }
  return url.href;
}
