import { Handlers, PageProps } from "$fresh/server.ts";
import SignupForm from "../islands/SignupForm.tsx";
import { encode } from "@std/encoding/base32.ts";
import PageState from "../utils/page-state.ts";

type SignupProps = {
  formId: string;
  csrfToken: string;
};

export const handler: Handlers<unknown, PageState> = {
  GET(_req, ctx) {
    ctx.state.title = "Création de compte";
    ctx.state.showNavbarActions = false;

    const formIdRaw = new Uint8Array(20);
    crypto.getRandomValues(formIdRaw);
    const formId = encode(formIdRaw);

    const csrfTokenRaw = new Uint8Array(20);
    crypto.getRandomValues(csrfTokenRaw);
    const csrfToken = encode(csrfTokenRaw);

    sessionStorage.setItem(formId, csrfToken);
    setTimeout(() => {
      sessionStorage.removeItem(formId);
    }, 5 * 60 * 1000);

    return ctx.render({ formId, csrfToken });
  },
};

export default function Signup({ data }: PageProps<SignupProps>) {
  const { csrfToken, formId } = data;
  return (
    <>
      <h1 class="my-5 text-xl md:text-2xl font-bold">
        Création de compte
      </h1>
      <SignupForm formId={formId} csrfToken={csrfToken} />
    </>
  );
}
