import { Handlers, PageProps, RouteConfig } from "$fresh/server.ts";
import { getFirebaseUserFromRequest } from "../utils/user.ts";
import { createUser, getUserById } from "../db/repositories/mod.ts";
import { getUserNamePattern, validateUserName } from "../utils/validation.ts";

type SetUserInfoPostBody = {
  name: string;
  redirect: string;
};

type SetUserInfoProps = {
  redirect: string;
};

export const config: RouteConfig = {
  skipInheritedLayouts: true,
};

export const handler: Handlers<SetUserInfoProps | null> = {
  GET(req, ctx) {
    const url = new URL(req.url);
    return ctx.render({ redirect: url.searchParams.get("redirect") ?? "/" });
  },
  async POST(req) {
    const formData = await req.formData();
    const redirect =
      (formData.get("redirect")?.valueOf() as string | undefined) ?? "/";

    const firebaseUser = await getFirebaseUserFromRequest(req);
    if (!firebaseUser) {
      return new Response(null, { status: 302, headers: { "Location": "/" } });
    }
    const userInfo = await getUserById(firebaseUser.user_id);
    if (userInfo?.name) {
      return new Response(null, {
        status: 302,
        headers: { "Location": redirect },
      });
    }

    const name = formData.get("name")?.valueOf() as string | undefined;
    if (!validateUserName(name)) {
      return new Response("Invalid name", { status: 403 });
    }

    await createUser({ uid: firebaseUser.user_id, name: name as string });

    return new Response(null, {
      status: 302,
      headers: {
        "Location": redirect ?? "/",
      },
    });
  },
};

export default function SetUserInfo({ data }: PageProps<SetUserInfoProps>) {
  const { redirect } = data;
  return (
    <div class="w-screen h-screen p-2 dark dark:bg-black dark:text-white">
      <div class="flex flex-col justify-center h-full md:w-3/12 md:m-auto">
        <p class="text-justify">
          C'est votre première connexion. Vous devez renseigner un nom
          d'utilisateur, après quoi il ne pourra plus être changé.
        </p>
        <form method="post">
          <div class="mt-5 flex flex-col gap-2 mx-auto">
            <label for="user-name" class="uppercase font-bold">Name</label>
            <div class="flex flex-col gap-1">
              <input
                type="text"
                name="name"
                id="user-name"
                required
                pattern={getUserNamePattern()}
                class="p-2 bg-gray-200 dark:bg-gray-700"
              />
              <p class="text-sm">
                Le nom d'utilisateur ne peut contenir que des lettres minuscules
                et des chiffres.
              </p>
            </div>
            <button
              type="submit"
              class="rounded mt-3 p-2 uppercase font-bold text-white bg-green-700"
            >
              Valider
            </button>
            <input type="hidden" name="redirect" value={redirect} />
          </div>
        </form>
      </div>
    </div>
  );
}
