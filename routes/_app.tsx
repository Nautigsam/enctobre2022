import { AppProps } from "$fresh/server.ts";
import { NavBar } from "../components/Navbar.tsx";
import PageState from "../utils/page-state.ts";
import UserContextProvider from "../utils/user-context.tsx";

export default function App(
  { Component, state }: AppProps<unknown, PageState>,
) {
  return (
    <html>
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Enctobre 2023{state.title ? ` - ${state.title}` : ""}</title>
      </head>
      <body class="min-h-screen dark dark:bg-black dark:text-white">
        <UserContextProvider value={state.user ?? null}>
          <Component />
        </UserContextProvider>
      </body>
    </html>
  );
}
