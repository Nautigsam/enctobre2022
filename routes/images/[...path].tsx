import { Handlers, RouteConfig } from "$fresh/server.ts";
import { typeByExtension } from "@std/media_types/mod.ts";
import { extname } from "@std/path/mod.ts";
import { encode as base32 } from "@std/encoding/base32.ts";
import {
  assertIsFolder,
  type ImageData,
  ImagesStore,
} from "../../utils/images-store.ts";

export const config: RouteConfig = {
  skipInheritedLayouts: true,
};

export const handler: Handlers = {
  async GET(req, ctx) {
    const { path } = ctx.params;

    const [folder, name] = path.split("/");

    let file: ImageData;
    try {
      assertIsFolder(folder);
      file = await ImagesStore.getData(folder, name);
    } catch {
      return new Response("Not found", { status: 404 });
    }

    let etag = null;

    if (file.mtime) {
      const encoder = new TextEncoder();
      etag = base32(
        new Uint8Array(
          await crypto.subtle.digest(
            "SHA-1",
            encoder.encode(file.mtime.toISOString()),
          ),
        ),
      );
    }

    const ifNoneMatch = req.headers.get("If-None-Match");
    if (ifNoneMatch && etag === ifNoneMatch) {
      return new Response(null, { status: 304 });
    }

    const headers: HeadersInit = {
      "Content-Type": typeByExtension(
        extname(name),
      ) ?? "appliction/octet-stream",
      "Content-Length": file.size.toString(),
      "Vary": "Accept-Encoding, If-None-Match",
    };
    if (etag) {
      headers["ETag"] = etag;
    }

    return new Response(file.readable, { headers });
  },
};
