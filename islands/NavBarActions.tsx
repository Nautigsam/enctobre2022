import { useState } from "preact/hooks";
import { User } from "../db/repositories/mod.ts";

type NavBarActionsProps = {
  user: User | null;
};

export default function NavBarActions(props: NavBarActionsProps) {
  const { user } = props;

  const [navOpen, setNavOpen] = useState(false);

  return user
    ? (
      <section class="md:pr-2">
        <div class="flex space-x-8 md:hidden">
          <a
            href="/posts/new"
            class="rounded p-2 bg-green-700 hover:bg-green-800 text-white"
          >
            Poster
          </a>
          <button
            onClick={() => setNavOpen(!navOpen)}
            class="text-2xl pr-3 focus:outline-none"
          >
            &#10296;
          </button>
        </div>
        <ul
          class={`left-2 right-2 text-center border-x border-b rounded ${
            navOpen ? "flex flex-col absolute" : "hidden"
          } md:left-auto md:right-auto md:flex md:flex-row md:gap-5 md:items-center md:border-none`}
        >
          <li class="hidden md:block">
            <a
              href="/posts/new"
              class="rounded p-2 bg-green-700 hover:bg-green-800 text-white"
            >
              Poster
            </a>
          </li>
          <li>
            <a
              href={`/users/${user.uid}`}
              class="block border-b py-3 bg-gray-200 hover:bg-gray-300 md:p-2 md:border md:rounded dark:bg-black dark:hover:bg-gray-800"
            >
              Mes images
            </a>
          </li>
          <li>
            <a
              href="/logout"
              class="block py-3 bg-gray-200 hover:bg-gray-300 md:p-2 md:border md:rounded dark:bg-black dark:hover:bg-gray-800"
            >
              Déconnexion
            </a>
          </li>
        </ul>
      </section>
    )
    : // No user logged in
      (
        <section>
          <a
            href="/login"
            class="border rounded p-2 bg-gray-200 hover:bg-gray-300 dark:bg-black dark:hover:bg-gray-800"
          >
            Connexion
          </a>
        </section>
      );
}
