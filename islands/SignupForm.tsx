import { useState } from "preact/hooks";

type SignupFormProps = {
  formId: string;
  csrfToken: string;
};

export default function SignupForm(props: SignupFormProps) {
  const { csrfToken, formId } = props;

  const [signupError, setSignupError] = useState("");

  function onSubmit(e: Event) {
    e.preventDefault();

    const formData = new FormData(e.target as HTMLFormElement);
    const email = formData.get("email")?.valueOf() as string;
    const password = formData.get("password")?.valueOf() as string;

    // @ts-ignore
    globalThis.createUserWithEmailAndPassword(globalThis.Auth, email, password)
      .then(
        // @ts-ignore
        ({ user }) => {
          return loginAndRedirect(user.accessToken);
        },
      )
      .catch((err: Error & { code?: string }) => {
        if (!err.code) {
          setSignupError(`Auth error (${err.name})`);
          return;
        }
        if (
          ["auth/invalid-email", "auth/wrong-password"].includes(
            err.code,
          )
        ) {
          setSignupError("Identifiants incorrects");
        } else {
          setSignupError(`Auth error (${err.code})`);
        }
      });
  }

  function loginAndRedirect(
    idToken: string,
  ) {
    return fetch("/login", {
      method: "POST",
      body: JSON.stringify({ idToken }),
      headers: {
        "Content-Type": "application/json",
        "Form-Id": formId,
        "Csrf-Token": csrfToken,
      },
    }).then(() => {
      window.location.assign("/");
    });
  }

  return (
    <div>
      <div class="flex flex-col gap-5 md:w-5/12 md:m-auto">
        {signupError
          ? (
            <div class="border-2 border-red-500 rounded p-2 font-bold text-sm bg-red-300 text-black">
              {signupError}
            </div>
          )
          : ""}
        <form onSubmit={onSubmit} class="border rounded p-2">
          <div class="mt-2 flex flex-col gap-2">
            <label for="signup-email" class="uppercase">Email</label>
            <input
              type="text"
              name="email"
              id="signup-email"
              class="p-2 bg-gray-200 dark:bg-gray-700"
            />
            <label for="signup-password" class="uppercase">
              Mot de passe
            </label>
            <input
              type="password"
              name="password"
              id="signup-password"
              class="p-2 bg-gray-200 dark:bg-gray-700"
            />
            <button
              type="submit"
              class="mt-2 rounded p-2 uppercase font-bold bg-green-700 text-white"
            >
              Valider
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
