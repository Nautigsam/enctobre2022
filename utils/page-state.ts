import { User } from "../db/repositories/mod.ts";

type PageState = {
  title?: string;
  user?: User;
  showNavbarActions?: boolean;
};

export default PageState;
