import { verifySessionCookie } from "../firebase-server.ts";

export async function getFirebaseUserFromRequest(req: Request) {
  const cookieRaw = req.headers.get("Cookie");
  if (!cookieRaw) return null;
  try {
    const cookies: Record<string, string> = Object.fromEntries(
      cookieRaw.split(";").map((e) => e.split("=").map((e) => e.trim())),
    );
    const sessionCookie = cookies["session"];
    if (!sessionCookie) return null;
    return await verifySessionCookie(sessionCookie);
  } catch (err) {
    console.error(new Error("Could not verify session", { cause: err }));
    return null;
  }
}
