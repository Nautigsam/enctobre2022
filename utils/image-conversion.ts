const dylib = Deno.dlopen(
  new URL(
    "libimage_conversion.so",
    import.meta.url,
  ),
  {
    "convert_image": {
      parameters: ["buffer", "usize", "u8", "pointer"],
      result: "u32",
      nonblocking: true,
    },
    "drop_image": {
      parameters: ["pointer", "usize"],
      result: "void",
    },
  },
);

export async function convertImage(
  src: Uint8Array,
  resize = false,
): Promise<Uint8Array> {
  // We create a BigInt64Array in order to get a pointer as a 64 bits value
  const dst = new BigInt64Array(1);
  const dstLen = (await dylib.symbols.convert_image(
    src,
    src.byteLength,
    resize ? 1 : 0,
    Deno.UnsafePointer.of(dst),
  )).valueOf();
  if (!dstLen) throw new Error("image conversion error");
  const dstDataPtr = new Deno.UnsafePointerView(
    Deno.UnsafePointer.create(dst[0]),
  );
  const dstData = dstDataPtr.getArrayBuffer(dstLen);
  return new Uint8Array(dstData);
}

export function freeImage(data: Uint8Array) {
  dylib.symbols.drop_image(Deno.UnsafePointer.of(data), data.byteLength);
}
