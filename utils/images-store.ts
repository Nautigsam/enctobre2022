import { ensureDir } from "@std/fs/mod.ts";
import { insert, remove } from "../firebase-server.ts";

type Folder = "originals" | "cropped";
export function assertIsFolder(input: unknown): asserts input is Folder {
  if (typeof input === "string" && ["originals", "cropped"].includes(input)) {
    return;
  }
  throw new Error("invalid folder");
}

export type ImageData = {
  mtime: Date | null;
  size: number;
  readable: ReadableStream;
};
type StoreType = "file" | "firebase";
type Store = {
  type: StoreType;
  getUrl(folder: Folder, name: string): URL;
  getData(folder: Folder, name: string): Promise<ImageData>;
  set(folder: Folder, name: string, data: Uint8Array): Promise<void>;
  remove(folder: Folder, name: string): Promise<void>;
};

class FileStore implements Store {
  readonly type = "file";

  constructor(base: URL) {
    this.#base = base;
  }

  getUrl(folder: Folder, name: string) {
    return new URL(`file:///images/${folder}/${name}`);
  }
  async getData(folder: Folder, name: string) {
    const file = await Deno.open(new URL(`${folder}/${name}`, this.#base), {
      read: true,
    });
    const stat = await file.stat();
    return { mtime: stat.mtime, size: stat.size, readable: file.readable };
  }
  async set(folder: Folder, name: string, data: Uint8Array): Promise<void> {
    const file = await Deno.open(new URL(`${folder}/${name}`, this.#base), {
      create: true,
      write: true,
    });
    await file.write(data);
    file.close();
  }
  async remove(folder: Folder, name: string): Promise<void> {
    await Deno.remove(new URL(`${folder}/${name}`, this.#base));
  }

  #base: URL;
}

class FirebaseStore implements Store {
  readonly type = "firebase";

  constructor(bucket: string) {
    this.#bucket = bucket;
  }

  getUrl(folder: Folder, name: string) {
    return new URL(
      `https://firebasestorage.googleapis.com/v0/b/${this.#bucket}/o/${
        encodeURIComponent(`${folder}/${name}`)
      }?alt=media`,
    );
  }
  getData(_folder: Folder, _name: string): Promise<ImageData> {
    // We don't need to get data for now. Only clients access directly to firebase.
    throw new Error("Method not implemented.");
  }
  set(folder: Folder, name: string, data: Uint8Array) {
    return insert(this.#bucket, `${folder}/${name}`, data);
  }
  remove(folder: Folder, name: string) {
    return remove(this.#bucket, `${folder}/${name}`);
  }

  #bucket: string;
}

let imagesStore: Store;
const bucketName = Deno.env.get("ENC22_FIREBASE_BUCKET");

if (bucketName) {
  console.log(`Using Firebase store with bucket "${bucketName}"`);
  imagesStore = new FirebaseStore(bucketName);
} else {
  const imagesStorePath = Deno.env.get("ENC22_IMG_STORE_PATH");

  let baseStore: URL;
  if (imagesStorePath) {
    baseStore = new URL(
      imagesStorePath.endsWith("/") ? imagesStorePath : imagesStorePath + "/",
    );
  } else {
    baseStore = new URL("../images/", import.meta.url);
  }

  console.log("Using file image store", baseStore.href);

  const OriginalsStore = new URL("originals/", baseStore);
  const CroppedStore = new URL("cropped/", baseStore);

  await ensureDir(OriginalsStore);
  await ensureDir(CroppedStore);

  imagesStore = new FileStore(baseStore);
}

export const ImagesStore = imagesStore;
