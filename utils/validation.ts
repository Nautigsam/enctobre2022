const USER_NAME_REGEX = /[a-z0-9]+/;

export function validateUserName(name?: string) {
  if (!name) return false;
  return USER_NAME_REGEX.test(name);
}
export function getUserNamePattern() {
  return regexToPattern(USER_NAME_REGEX);
}

function regexToPattern(regexp: RegExp) {
  const fullStr = regexp.toString();
  const firstSlash = fullStr.indexOf("/");
  const lastSlash = fullStr.lastIndexOf("/");
  return fullStr.slice(firstSlash + 1, lastSlash);
}
