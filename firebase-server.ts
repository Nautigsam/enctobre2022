import { Buffer } from "node:buffer";
import {
  decodeProtectedHeader,
  importPKCS8,
  importX509,
  JWTPayload,
  jwtVerify,
  SignJWT,
} from "jose";
import { Storage } from "@google-cloud/storage";

type Credentials = {
  project_id: string;
  private_key: string;
  client_email: string;
};
let credentials = {
  project_id: Deno.env.get("ENC22_FIREBASE_PROJECT_ID"),
  private_key: Deno.env.get("ENC22_FIREBASE_PRIVATE_KEY"),
  client_email: Deno.env.get("ENC22_FIREBASE_CLIENT_EMAIL"),
};

function areCredentialsComplete(
  input: Partial<Credentials>,
): input is Credentials {
  return Boolean(
    input.project_id && input.private_key &&
      input.client_email,
  );
}

if (!areCredentialsComplete(credentials)) {
  try {
    const filePath = Deno.env.get("GOOGLE_APPLICATION_CREDENTIALS");
    if (!filePath) {
      throw "crap";
    }
    credentials = JSON.parse(await Deno.readTextFile(filePath));
    if (!areCredentialsComplete(credentials)) {
      throw "crap";
    }
  } catch {
    console.error(
      "Unable to read Firebase credentials. You need to set variables for each value of use GOOGLE_APPLICATION_CREDENTIALS.",
    );
    Deno.exit(1);
  }
}

const privkey = await importPKCS8(credentials.private_key, "RS256");

let GooglePubKeys = await fetchGooglePublicKeys();
async function getGooglePublicKeys() {
  if (GooglePubKeys.expirationDate.getTime() <= (new Date()).getTime()) {
    GooglePubKeys = await fetchGooglePublicKeys();
  }
  return GooglePubKeys.data;
}

export async function createSessionCookie(
  idToken: string,
  validDurationSec: number,
) {
  const res = await doAuthRequest(":createSessionCookie", {
    method: "POST",
    body: JSON.stringify({
      idToken,
      validDuration: validDurationSec.toString(),
    }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  const resBody: { sessionCookie?: string } = await res.json();
  if (!resBody.sessionCookie) {
    console.error(
      `[AUTH] "POST :createSessionCookie" ${res.status} "${
        JSON.stringify(resBody)
      }"`,
    );
  }
  return resBody.sessionCookie;
}

async function doAuthRequest(api: string, init: RequestInit = {}) {
  const newInit: RequestInit = {
    ...init,
    headers: {
      ...(init.headers ?? {}),
      "Authorization": `Bearer ${await getAccessToken()}`,
    },
  };
  return fetch(
    `https://identitytoolkit.googleapis.com/v1/projects/${credentials.project_id}${api}`,
    newInit,
  );
}

type FirebaseSessionClaims = {
  auth_time: number;
  user_id: string;
};
export type FirebaseUser = JWTPayload & FirebaseSessionClaims;
export async function verifySessionCookie(
  sessionCookie: string,
): Promise<FirebaseUser | null> {
  const header = decodeProtectedHeader(sessionCookie);
  if (!header.kid || !header.alg) return null;

  const allPubkeys = await getGooglePublicKeys();
  const pubkeyRaw = allPubkeys[header.kid];
  if (!pubkeyRaw) return null;

  try {
    const pubkey = await importX509(pubkeyRaw, header.alg);
    const { payload } = await jwtVerify(sessionCookie, pubkey, {
      audience: credentials.project_id,
      issuer: `https://session.firebase.google.com/${credentials.project_id}`,
    });
    return payload as FirebaseUser;
  } catch {
    return null;
  }
}

export async function insert(bucket: string, path: string, data: Uint8Array) {
  const storage = new Storage({ token: await getAccessToken() });

  await storage.bucket(bucket).file(path).save(Buffer.from(data));
}

export async function remove(bucket: string, path: string) {
  const storage = new Storage({ token: await getAccessToken() });

  await storage.bucket(bucket).file(path).delete({ ignoreNotFound: true });
}

type ServiceAccountToken = {
  accessToken: string;
  expirationTime: number; // in seconds
};

let Token: ServiceAccountToken | null = null;

async function getAccessToken() {
  // We cheat on current date to be sure the token is always valid
  if (!Token || Token.expirationTime < (Date.now() + 60)) {
    Token = await createAccessToken();
  }
  return Token.accessToken;
}

// See https://github.com/firebase/firebase-admin-node/blob/318f0e43d65f47dfae81decf5166ddd198d99680/src/app/credential-internal.ts#L94
async function createAccessToken(): Promise<ServiceAccountToken> {
  const header = {
    typ: "JWT",
    alg: "RS256",
  };
  const claims = {
    // audience
    aud: "https://accounts.google.com/o/oauth2/token",
    // issuer
    iss: credentials.client_email,
    scope: [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/firebase.database",
      "https://www.googleapis.com/auth/firebase.messaging",
      "https://www.googleapis.com/auth/identitytoolkit",
      "https://www.googleapis.com/auth/userinfo.email",
    ].join(" "),
  };
  const jwt = await new SignJWT(claims)
    .setProtectedHeader(header)
    .setIssuedAt()
    .setExpirationTime("1h")
    .sign(privkey);

  const body = `grant_type=${
    encodeURIComponent(
      "urn:ietf:params:oauth:grant-type:jwt-bearer",
    )
  }&assertion=${jwt}`;
  const res = await fetch("https://accounts.google.com/o/oauth2/token", {
    method: "POST",
    body,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  });

  const tokenInfo: { access_token: string; expires_in: number } = await res
    .json();

  return {
    accessToken: tokenInfo.access_token,
    expirationTime: Date.now() + tokenInfo.expires_in,
  };
}

async function fetchGooglePublicKeys() {
  const res = await fetch(
    "https://www.googleapis.com/identitytoolkit/v3/relyingparty/publicKeys",
  );
  const data = await res.json() as Record<string, string>;
  const [, maxAgeSec] = res.headers.get("Cache-Control")?.split(",").map(
    (a) => a.trim(),
  ).find((a) => a.startsWith("max-age"))?.split("=") as [string, string];
  const resDate = new Date(res.headers.get("Date") as string);
  resDate.setSeconds(resDate.getSeconds() + parseInt(maxAgeSec));
  return { expirationDate: resDate, data };
}
