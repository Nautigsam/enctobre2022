import { useContext } from "preact/hooks";
import PostActions from "../islands/PostActions.tsx";
import { UserContext } from "../utils/user-context.tsx";

export interface PostsGridItem {
  name: string;
  author_id: string;
  author: string;
  day: number;
  text: string;
  link: string;
  isLiked: boolean;
  likesCount: number;
  originalUrl: URL;
  croppedUrl: URL;
}

type PostsGridProps = {
  posts: PostsGridItem[];
};

export function PostsGrid(
  { posts }: PostsGridProps,
) {
  const user = useContext(UserContext);

  return (
    <ul class="grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-2">
      {posts.map((post) => {
        const pageUrl = `/posts/${post.name}`;

        return (
          <li
            key={post.name}
            class="rounded border p-2 hover:shadow-lg"
          >
            <a href={pageUrl}>
              <picture>
                <source
                  media="(min-width: 768px)"
                  srcset={stripHostnameIfFile(post.croppedUrl)}
                />
                <img
                  src={stripHostnameIfFile(post.originalUrl)}
                  alt={`The image uploaded by ${post.author} on day ${post.day}`}
                />
              </picture>
            </a>
            <div class="mt-3 flex justify-between items-center">
              <a href={post.link} class="block underline">
                {post.text}
              </a>
              <PostActions
                post={post}
                canRemove={user?.uid === post.author_id}
                canLike={user != null}
              />
            </div>
          </li>
        );
      })}
    </ul>
  );
}

function stripHostnameIfFile(url: URL) {
  if (url.protocol === "file:") {
    return url.pathname;
  }
  return url.href;
}
